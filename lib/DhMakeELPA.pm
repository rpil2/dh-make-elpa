package DhMakeELPA;

use warnings;
use strict;

use base 'Class::Accessor';

__PACKAGE__->mk_accessors( qw( cfg ) );

=head1 NAME

DhMakeELPA - create Debian source package from Emacs Lisp source code

=head1 SYNOPSIS

    use DhMakeELPA;

    DhMakeELPA->run;

=head1 ACCESSORS

=over

=item cfg

Stores the configuration, an instance of L<DhMakeELPA::Config>

=back

=cut

use DhMakeELPA::Config;

=item run( I<%init> )

Runs DhMakeELPA.

Unless the %init contains an I<cfg> member, constructs and instance of
L<DhMakeELPA::Config> and assigns it to I<$init{cfg}>.

Then determines the dh-make-elpa command requested (via cfg->command),
loads the appropriate I<DhMakeELPA::Command::$command> class,
constructs an instance of it and calls its I<execute> method.

=cut

sub run {
    my ( $class, %c ) = @_;

    unless ( $c{cfg} ) {
        my $cfg = DhMakeELPA::Config->new;
        $cfg->parse_command_line_options;
        $c{cfg} = $cfg;
    }

    my $cmd_mod = $c{cfg}->command;
    $cmd_mod =~ s/-/_/g;
    require "DhMakeELPA/Command/$cmd_mod.pm";

    $cmd_mod =~ s{/}{::}g;
    $cmd_mod = "DhMakeELPA::Command::$cmd_mod";

    my $self = $cmd_mod->new( \%c );

    return $self->execute;
}

=back

=head1 COPYRIGHT & LICENSE

=over

=item Copyright (C) 2016-2018 Sean Whitton <spwhitton@spwhitton.name>

=back

It is heavily based on the corresponding part of dh-make-perl, which
is originally copyrighted as follows:

=over

=item Copyright (C) 2009, 2010 Damyan Ivanov <dmn@debian.org>

=back

This program is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License version 2 as published by the Free
Software Foundation.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program; if not, write to the Free Software Foundation, Inc., 51 Franklin
Street, Fifth Floor, Boston, MA 02110-1301 USA.

=cut

1;
