Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: dh-make-elpa
Upstream-Contact: Debian Emacsen team <debian-emacsen@lists.debian.org>
Source: https://salsa.debian.org/emacsen-team/dh-make-elpa
Comment:
 Parts of the dh-make-elpa code are based on the code of dh-make-perl,
 written and maintained by the Debian Perl Group. All the dh-make-elpa
 files also contain a copyright information from the corresponding
 dh-make-perl parts.

Files: *
Copyright: (C) 2016-2018 Sean Whitton <spwhitton@spwhitton.name>
License: GPL-2

Files: lib/DhMakeELPA/Config.pm
       lib/DhMakeELPA/Command/make.pm
       lib/DhMakeELPA/Command/Packaging.pm
       debian/*
Copyright: (C) 2016-2018 Sean Whitton <spwhitton@spwhitton.name>
           (C) 2018-2021 Lev Lamberov <dogsleg@debian.org>
License: GPL-2

Files: lib/DhMakeELPA/Command/help.pm
Copyright: (C) 2018-2020 Lev Lamberov <dogsleg@debian.org>
License: GPL-2

License: GPL-2
 dh-make-elpa is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, version 2 dated June, 1991.
 .
 dh-make-elpa is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with dh-make-elpa.  If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2.
